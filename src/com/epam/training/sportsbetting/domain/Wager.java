package com.epam.training.sportsbetting.domain;

import com.epam.training.sportsbetting.ui.OutcomeOddView;

import java.sql.Timestamp;

public class Wager {
  private Player player;
  private OutcomeOdd outcomeOdd;
  private Integer amount;
  private String currency;
  private Long timestamp;
  private Boolean isProcessed = false;
  private Boolean win;
  private OutcomeOddView outcomeOddView;

  public Wager(Player player, OutcomeOdd outcomeOdd, Integer amount, String currency, OutcomeOddView outcomeOddView) {
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    this.player = player;
    this.outcomeOdd = outcomeOdd;
    this.amount = amount;
    this.currency = currency;
    this.timestamp = timestamp.getTime();
    this.outcomeOddView = outcomeOddView;
  }

  public OutcomeOdd getOutcomeOdd() {
    return outcomeOdd;
  }

  public void markProcessed() {
    isProcessed = true;
  }

  public void markWin() {
    win = true;
  }

  public Double getWinSize() {
    return amount * outcomeOdd.getOddValue();
  }

  public String getCurrency() {
    return currency;
  }

  public OutcomeOddView getOutcomeOddView() {
    return outcomeOddView;
  }
}
