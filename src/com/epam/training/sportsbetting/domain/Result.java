package com.epam.training.sportsbetting.domain;

import java.util.ArrayList;
import java.util.List;

public class Result {
  private List<Outcome> outcomesList = new ArrayList<>();
  void addOutcome(Outcome outcome) {
    outcomesList.add(outcome);
  }

  public List<Outcome> getOutcomesList() {
    return outcomesList;
  }
}
