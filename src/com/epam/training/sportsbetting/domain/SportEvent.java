package com.epam.training.sportsbetting.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class SportEvent {
  private String title;
  private LocalDateTime startDate;
  private LocalDateTime endDate;
  private List<Bet> betList= new ArrayList<>();

  SportEvent(String title, LocalDateTime startDate, LocalDateTime endDate) {
    this.title = title;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public void addBet(Bet bet) {
    betList.add(bet);
    bet.setSportEvent(this);
  }

  public String getTitle() {
    return title;
  }

  public List<Bet> getBetList() {
    return betList;
  }

  public Result generateResult() {
    Integer min = 0;
    Random rand = new Random();
    Result result = new Result();

    for (Bet bet : betList) {
      List<Outcome> outcomesList= bet.getOutcomesList();

      int randomNum = rand.nextInt(outcomesList.size() - min) + min;
      result.addOutcome(outcomesList.get(randomNum));
    }

    return result;
  }
}
