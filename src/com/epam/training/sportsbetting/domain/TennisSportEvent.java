package com.epam.training.sportsbetting.domain;

import java.time.LocalDateTime;

public class TennisSportEvent extends SportEvent {
  private final String EventType = "TennisSportEvent";

  public TennisSportEvent(String title, LocalDateTime startDate, LocalDateTime endDate) {
    super(title, startDate, endDate);
  }
}
