package com.epam.training.sportsbetting.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class OutcomeOdd {
  private Outcome outcome;
  private Double oddValue;
  private LocalDateTime validFrom;
  private LocalDateTime validTo;

  public OutcomeOdd(Double oddValue, LocalDateTime validFrom, LocalDateTime validTo) {
    this.oddValue = oddValue;
    this.validFrom = validFrom;
    this.validTo = validTo;
  }

  void setOutcome(Outcome outcome) {
    this.outcome = outcome;
  }

  public Double getOddValue() {
    return oddValue;
  }

  public LocalDateTime getValidFrom() {
    return validFrom;
  }

  public LocalDateTime getValidTo() {
    return validTo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OutcomeOdd that = (OutcomeOdd) o;
    return Objects.equals(outcome, that.outcome) &&
        Objects.equals(oddValue, that.oddValue) &&
        Objects.equals(validFrom, that.validFrom) &&
        Objects.equals(validTo, that.validTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(outcome, oddValue, validFrom, validTo);
  }
}
