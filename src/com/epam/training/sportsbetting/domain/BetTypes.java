package com.epam.training.sportsbetting.domain;

public enum  BetTypes {
  Goals, Winner, Score
}
