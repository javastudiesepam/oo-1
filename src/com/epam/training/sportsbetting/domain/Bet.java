package com.epam.training.sportsbetting.domain;

import java.util.ArrayList;
import java.util.List;

public class Bet {
  private SportEvent sportEvent;
  private String description;
  private List<Outcome> outcomesList = new ArrayList<>();
  private BetTypes type;

  public Bet(String description, BetTypes type) {
    this.description = description;
    this.type = type;
  }

  void setSportEvent(SportEvent sportEvent) {
    this.sportEvent = sportEvent;
  }

  public void addOutcome(Outcome outcome) {
    outcomesList.add(outcome);
  }

  public List<Outcome> getOutcomesList() {
    return outcomesList;
  }

  public BetTypes getType() {
    return type;
  }
}
