package com.epam.training.sportsbetting.domain;

import java.time.LocalDate;

public class Player {
  private String name;
  private String accountNumber;
  private Integer balance;
  private String currency;
  private LocalDate birthDate;

  public Player(String name, String accountNumber, Integer balance, String currency, LocalDate birthDate) {
    this.name = name;
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.currency = currency;
    this.birthDate = birthDate;
  }

  public Integer decreaseBalance(Integer size) {
    if (balance >= size) {
      balance = balance - size;
      return balance;
    }
    return null;
  }

  public Integer getBalance() {
    return balance;
  }

  public String getCurrency() {
    return currency;
  }
}
