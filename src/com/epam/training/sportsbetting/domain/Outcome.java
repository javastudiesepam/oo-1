package com.epam.training.sportsbetting.domain;

import java.util.ArrayList;
import java.util.List;


public class Outcome {
  private String value;
  private List<OutcomeOdd> outcomeOddList = new ArrayList<>();

  public Outcome(String value) {
    this.value = value;
  }

  public void addOutcomeOdd(OutcomeOdd outcomeOdd) {
    outcomeOddList.add(outcomeOdd);
    outcomeOdd.setOutcome(this);
  }

  public List<OutcomeOdd> getOutcomeOddList() {
    return outcomeOddList;
  }

  public String getValue() {
    return value;
  }
}
