package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbetting.domain.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// initializes objects with predefined data
public class InitService {
  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy HH:mm");

  public static SportEvent initEvent() {
    // init event
    LocalDateTime startDate = LocalDateTime.parse("27 Oct 2016 19:00", formatter);
    LocalDateTime endDate = LocalDateTime.parse("27 Oct 2016 21:00", formatter);
    SportEvent footballSportEvent = new FootballSportEvent("Southampton v Bournemouth", startDate, endDate);

    // add bets
    footballSportEvent.addBet(initWinnerBet());
    footballSportEvent.addBet(initGoalsBet());

    return footballSportEvent;
  }

  private static Bet initWinnerBet() {
    // init outcomes
    Outcome outcomeFirstWon = new Outcome("Southampton");
    Outcome outcomeSecondWon = new Outcome("Bournemouth");
    Outcome outcomeDraw = new Outcome("Draw");

    // add outcomeOdds
    outcomeFirstWon.addOutcomeOdd(
        new OutcomeOdd(
            4.0,
            LocalDateTime.parse("27 Sep 2016 19:00", formatter),
            LocalDateTime.parse("30 Sep 2016 18:59", formatter)
        )
    );
    outcomeFirstWon.addOutcomeOdd(
        new OutcomeOdd(
            5.0,
            LocalDateTime.parse("30 Sep 2016 19:00", formatter),
            LocalDateTime.parse("02 Oct 2016 18:59", formatter)
        )
    );

    outcomeSecondWon.addOutcomeOdd(
        new OutcomeOdd(
            1.7,
            LocalDateTime.parse("27 Sep 2016 19:00", formatter),
            LocalDateTime.parse("30 Sep 2016 18:59", formatter)
        )
    );
    outcomeSecondWon.addOutcomeOdd(
        new OutcomeOdd(
            1.5,
            LocalDateTime.parse("30 Sep 2016 19:00", formatter),
            LocalDateTime.parse("02 Oct 2016 18:59", formatter)
        )
    );

    outcomeDraw.addOutcomeOdd(
        new OutcomeOdd(
            3.0,
            LocalDateTime.parse("27 Sep 2016 19:00", formatter),
            LocalDateTime.parse("30 Sep 2016 18:59", formatter)
        )
    );
    outcomeDraw.addOutcomeOdd(
        new OutcomeOdd(
            3.5,
            LocalDateTime.parse("30 Sep 2016 19:00", formatter),
            LocalDateTime.parse("02 Oct 2016 18:59", formatter)
        )
    );

    // init bet
    Bet bet = new Bet("", BetTypes.Winner);

    // add outcomes
    bet.addOutcome(outcomeFirstWon);
    bet.addOutcome(outcomeSecondWon);
    bet.addOutcome(outcomeDraw);

    return bet;
  }

  private static Bet initGoalsBet() {
    // init outcomes
    Outcome outcome1 = new Outcome("1");
    Outcome outcome2 = new Outcome("2");
    Outcome outcome3 = new Outcome(">=2");

    // add outcomeOdds
    outcome1.addOutcomeOdd(
        new OutcomeOdd(
            1.75,
            LocalDateTime.parse("27 Sep 2016 19:00", formatter),
            LocalDateTime.parse("30 Sep 2016 18:59", formatter)
        )
    );
    outcome2.addOutcomeOdd(
        new OutcomeOdd(
            1.25,
            LocalDateTime.parse("30 Sep 2016 19:00", formatter),
            LocalDateTime.parse("02 Oct 2016 18:59", formatter)
        )
    );
    outcome3.addOutcomeOdd(
        new OutcomeOdd(
            1.05,
            LocalDateTime.parse("27 Sep 2016 19:00", formatter),
            LocalDateTime.parse("30 Sep 2016 18:59", formatter)
        )
    );

    // init bet
    Bet bet = new Bet("", BetTypes.Goals);

    // add outcomes
    bet.addOutcome(outcome1);
    bet.addOutcome(outcome2);
    bet.addOutcome(outcome3);

    return bet;
  }
}
