package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.ui.OutcomeOddView;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

public class DialogService {
  public static OutcomeOddView getPlayersChoice(BufferedReader br, List<OutcomeOddView> outcomeOddViewList) throws IOException {
    int variant = 0;
    while (true) {
      System.out.println("What do you want to bet on? (choose a number or press q for quit)");
      for (int i=0; i < outcomeOddViewList.size(); i++) {
        System.out.println(i+1 + ": " + outcomeOddViewList.get(i));
      }

      try {
        String line = br.readLine();

        if (line.equals("q")) {
          return null;
        }

        variant = Integer.parseInt(line);
      } catch (NumberFormatException e) {
        System.out.println("Incorrect option chosen, try again");
      }

      if (variant > 0 && variant <= outcomeOddViewList.size()) {
        break;
      } else {
        System.out.println("Please try selecting a number within the range");
      }
    }

    return outcomeOddViewList.get(variant - 1);
  }

  public static Integer getPlayersBetSize(BufferedReader br) throws IOException {
    int size = 0;
    System.out.println("How much do you want to bet on it? (q for quit)");
    while (true) {

      try {
        String line = br.readLine();

        if (line.equals("q")) {
          return null;
        }

        size = Integer.parseInt(line);
      } catch (NumberFormatException e) {
        System.out.println("Something went wrong, try again");
      }

      if (size > 0) {
        break;
      } else {
        System.out.println("Please try selecting a number greater then 0");
      }
    }
    return size;
  }

  public static Player getPlayerData(BufferedReader br) throws IOException {
    String name = getPlayerName(br);
    String accountNumber = getAccount(br);
    Integer balance = getBalance(br);
    String currency = getCurrency(br);
    LocalDate birthDate = getBirthDate(br);

    System.out.println("Welcome " + name);
    System.out.println("Your balance is " + balance + " " + currency);

    return new Player(name, accountNumber, balance, currency, birthDate);
  }

  private static String getPlayerName(BufferedReader br) throws IOException {
    System.out.println("Hi, what is your name?");
    return br.readLine();
  }

  private static String getAccount(BufferedReader br) throws IOException {
    System.out.println("What is your account number?");
    return br.readLine();
  }

  private static Integer getBalance(BufferedReader br) throws IOException {
    int balance = 0;

    while (true) {
      System.out.println("How much money do you have (more than 0)?");
      try {
        balance = Integer.parseInt(br.readLine());
      } catch (NumberFormatException e) {
        System.out.println("Incorrect format, try again");
      }
      if (balance > 0) {
        break;
      }
    }
    return balance;
  }

  private static String getCurrency(BufferedReader br) throws IOException {
    String currency;

    while (true) {
      System.out.println("What is your currency? (UAH, EUR or USD)");
      currency = br.readLine();
      if (currency.equals("UAH") || currency.equals("EUR") || currency.equals("USD")) {
        break;
      }
    }

    return currency;
  }

  private static LocalDate getBirthDate(BufferedReader br) throws IOException {
    LocalDate birthDate = null;

    while (true) {
      System.out.println("When were you born? eg.:1990-02-03");
      try {
        birthDate = LocalDate.parse(br.readLine());
      } catch (DateTimeParseException e) {
        System.out.println("Incorrect format, try again");
      }
      if (birthDate != null) {
        break;
      }
    }

    return birthDate;
  }
}