package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbetting.domain.*;
import com.epam.training.sportsbetting.ui.OutcomeOddView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class BettingService {
  public static List<OutcomeOddView> getOddViewList(SportEvent event) {
    List<OutcomeOddView> outcomeOddViewList = new ArrayList<>();

    String title = event.getTitle();

    // this iteration sucks at least it doesn't matter in this case
    for (Bet bet : event.getBetList()) {
      BetTypes betType = bet.getType();

      for (Outcome outcome : bet.getOutcomesList()) {
        String outcomeValue = outcome.getValue();

        for (OutcomeOdd outcomeOdd : outcome.getOutcomeOddList()) {
          Double oddValue = outcomeOdd.getOddValue();
          LocalDateTime validFrom = outcomeOdd.getValidFrom();
          LocalDateTime validTo = outcomeOdd.getValidTo();

          outcomeOddViewList.add(
              new OutcomeOddView(title, betType, outcomeValue, oddValue, validFrom, validTo, outcomeOdd)
          );
        }
      }
    }

    return outcomeOddViewList;
  }

  public static List<Wager> getWinningWagers(SportEvent event, List<Wager> wagerList) {
    List<Outcome> resultOutcomeList = event.generateResult().getOutcomesList();

    List<OutcomeOdd> winningOddList = resultOutcomeList
        .stream()
        .map(Outcome::getOutcomeOddList)
        .flatMap(Collection::stream)
        .collect(Collectors.toList());

    return wagerList
        .stream()
        .peek(Wager::markProcessed)
        .filter(wager -> winningOddList.contains(wager.getOutcomeOdd()))
        .peek(Wager::markWin)
        .collect(Collectors.toList());
  }

  public static void printGameOutcome(Player player, List<Wager> winningWagerList) {
    if (winningWagerList.size() != 0) {
      Double prize = winningWagerList
          .stream()
          .map(Wager::getWinSize)
          .reduce((s1, s2) -> s1 + s2)
          .orElse(0.0);

      StringBuilder sb = new StringBuilder();
      winningWagerList
          .stream()
          .map(Wager::getOutcomeOddView)
          .forEach(sb::append);

      System.out.println("Results:");
      System.out.println("The winner is " + sb);
      System.out.println("You have won " + prize + " " + player.getCurrency());
    } else {
      System.out.println("Unfortunately, you haven't won anything");
    }
  }
}
