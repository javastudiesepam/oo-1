package com.epam.training.sportsbetting;

import com.epam.training.sportsbetting.domain.*;
import com.epam.training.sportsbetting.service.BettingService;
import com.epam.training.sportsbetting.service.DialogService;
import com.epam.training.sportsbetting.service.InitService;
import com.epam.training.sportsbetting.ui.OutcomeOddView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class App {

  public static void main(String[] args) {
    SportEvent sportEvent = InitService.initEvent();
    Player player = null;
    List<OutcomeOddView> outcomeOddViewList = BettingService.getOddViewList(sportEvent);
    List<Wager> wagerList = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {

      player = DialogService.getPlayerData(br);

      while (true) {

        OutcomeOddView oddView = DialogService.getPlayersChoice(br, outcomeOddViewList);

        if (oddView == null) {
          break;
        }

        Integer betSize = DialogService.getPlayersBetSize(br);

        if (betSize == null) {
          break;
        }

        Integer newBalance = player.decreaseBalance(betSize);

        if (newBalance != null) {
          System.out.println("Your new balance is " + player.getBalance() + " " + player.getCurrency());

          wagerList.add(new Wager(player, oddView.getOutcomeOdd(), betSize, player.getCurrency(), oddView));
        } else {
          System.out.println("You don't have enough money, your balance is " + player.getBalance() + " "
              + player.getCurrency());
        }
      }
    } catch (IOException e) {
      System.out.println("something went wrong");
      System.exit(1);
    }

    List<Wager> winningWagerList = BettingService.getWinningWagers(sportEvent, wagerList);

    winningWagerList.forEach(System.out::println);

    BettingService.printGameOutcome(player, winningWagerList);

    System.out.println("Thank you for playing");
  }
}

