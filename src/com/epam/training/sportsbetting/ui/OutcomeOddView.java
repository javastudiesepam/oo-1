package com.epam.training.sportsbetting.ui;

import com.epam.training.sportsbetting.domain.BetTypes;
import com.epam.training.sportsbetting.domain.OutcomeOdd;

import java.time.LocalDateTime;

public class OutcomeOddView {
  private String title;
  private BetTypes betType;
  private String outcomeValue;
  private Double oddValue;
  private LocalDateTime validFrom;
  private LocalDateTime validTo;
  private OutcomeOdd outcomeOdd;

  public OutcomeOddView(String title, BetTypes betType, String outcomeValue, Double oddValue, LocalDateTime validFrom, LocalDateTime validTo, OutcomeOdd outcomeOdd) {
    this.title = title;
    this.betType = betType;
    this.outcomeValue = outcomeValue;
    this.oddValue = oddValue;
    this.validFrom = validFrom;
    this.validTo = validTo;
    this.outcomeOdd = outcomeOdd;
  }

  @Override
  public String toString() {
    return "Bet on the " + title + " sport event, the " + betType + " will be " + outcomeValue +
        ". The odd on this is " + oddValue + ", valid from " + validFrom + " to " + validTo;
  }

  public OutcomeOdd getOutcomeOdd() {
    return outcomeOdd;
  }
}
